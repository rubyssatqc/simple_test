require_relative '../libs/string_helper'
require_relative '../libs/results'

module SimpleTest
  class Core

    class << self

      def do_test(name, &block)
        status = execute_test(&block) ? 'PASSED'.green : 'FAILED'.red
        print_status(name, status)
        SimpleTest::Result.instance.add_test(name, status)
      end

      private

      def execute_test &block
        block.call
      end

      def print_status(name, status)
        puts "#{'TEST'.blue}: #{name} - #{status}"
      end
    end
  end
end
