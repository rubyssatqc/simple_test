require 'singleton'

module SimpleTest

  class Result

    include Singleton
    attr_accessor :result

    def initialize
      @result = {}
      @result[:start_time] = Time.now
    end

    def add_test(name, status)
      @result[:tests] ||= []
      @result[:tests] << {name: name, status: status}
    end

    def test_count
      @result[:tests].count
    end

    def failed_count
      @result[:tests].select{|x| x[:status].include?('FAILED')}.count
    end

    def passed_count
      test_count - failed_count
    end

    def puts_finish_result
      take_time = (Time.now - @result[:start_time]).to_f.round(4)
      puts "    Finished in #{take_time} seconds".green
      puts "#{'    Result:'.green} #{test_count.to_s.green} (#{passed_count.to_s.green}/#{failed_count.to_s.red})"
    end
  end
end
