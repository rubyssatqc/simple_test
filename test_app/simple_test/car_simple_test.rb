require_relative '../../../simple_test/core/simple_test'
require_relative '../car'

at_exit do
  SimpleTest::Result.instance.puts_finish_result
end

SimpleTest::Core.do_test 'Ensure that car name can be created correctly' do
  c = Car.new(120, 'Lada', 5)
  c.name.include?('La')
end

SimpleTest::Core.do_test 'Ensure that car speed can be created correctly' do
  c = Car.new(120, 'Lada', 5)

  #ensure_that(c.speed).is(120)
  true
end

SimpleTest::Core.do_test 'Ensure that car places can be created correctly' do
  c = Car.new(120, 'Lada', 5)
  c.places == 4
end


SimpleTest::Core.do_test 'one - passes' do
 1
end

SimpleTest::Core.do_test 'true - passes' do
  true
end

SimpleTest::Core.do_test 'nil - fails' do
 nil
end

SimpleTest::Core.do_test 'false - fails' do
 false
end
