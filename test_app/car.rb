class Car

  attr_accessor :speed,:name, :places

  def initialize(speed, name, places)
    @speed, @name, @places = speed.to_f, name, places
  end
end
