require_relative 'car'

class FastCar < Car
  def initialize(speed=200, name='Ferarry', places=2)
    @speed, @name, @places = speed, name, places
  end
end
